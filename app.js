$.ajaxSetup({ cache: false });

mainapp=angular
    .module('app', ['ui.router', "app.student","app.mercyCorps","app.program","app.createStudent",'app.beneficiary','app.beneficiaryCreate','app.data.beneficiary', 'app.data.student', 'app.data.ncl','app.program-detail',"ngTable"])
    .config(["$urlRouterProvider", function($urlRouterProvider){
        $urlRouterProvider.otherwise('/');
    }]);

mainapp.directive('tooltip', function(){
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            $(element).hover(function(){
                // on mouseenter
                $(element).tooltip('show');
            }, function(){
                // on mouseleave
                $(element).tooltip('hide');
            });
        }
    };
});


mainapp.directive('ngConfirmClick',[
    function(){
        return{
            link: function (scope, element, attr) {
                var msg = attr.ngConfirmClick || "Are you sure?";
                var clickAction = attr.confirmedClick;
                element.bind('click', function (event) {
                    if(window.confirm(msg)){
                        scope.$eval(clickAction)
                    }
                });
            }
        };
    }]);

mainapp.filter('capitalize', function() {
    return function(input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});