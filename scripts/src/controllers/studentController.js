aci=angular
    .module('app.student', ['ui.router','app.data.student','ui.bootstrap'])
    .config(["$stateProvider", function($stateProvider) {
        $stateProvider.state({
            name: 'student',
            url: '/',
            templateUrl: 'scripts/src/templates/student.html',
            controller: "studentController",
            resolve: {
                students: ['studentDataService', function(studentDataService) {
                    return studentDataService.getAllStudents();
                }]
                },
        });
    }])
    .controller("studentController", ["$scope", "$sce", "$http", "students","studentDataService", function($scope,$sce,$http, students,studentDataService) {
        $scope.projectName="Conditional Aid - Student Page";
        $scope.model={};
        $scope.model.students = students._embedded.student;
        $scope.model.attendedDate = new Date();
        $scope.tabs = [

            { title:'View Transactions', content:'Transactions' },
            { title:'Track Aid', content:'Track Aid' },
            { title:'Special Cases', content:'Health Issue or Any Special Case can be requested with proper proof' }
        ];


        $scope.trustHtml = function(content){
            return $sce.trustAsHtml( content );
        }
        $scope.today = function() {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.clear = function() {
            $scope.dt = null;
        };

        $scope.options = {
            customClass: getDayClass,
            minDate: new Date(),
            showWeeks: true
        };

        // Disable weekend selection
        function disabled(data) {
            var date = data.date,
                mode = data.mode;
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        }

        $scope.toggleMin = function() {
            $scope.options.minDate = $scope.options.minDate ? null : new Date();
        };

        $scope.toggleMin();

        $scope.setDate = function(year, month, day) {
            $scope.dt = new Date(year, month, day);
        };

        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date(tomorrow);
        afterTomorrow.setDate(tomorrow.getDate() + 1);
        $scope.events = [
            {
                date: tomorrow,
                status: 'full'
            },
            {
                date: afterTomorrow,
                status: 'partially'
            }
        ];

        function getDayClass(data) {
            var date = data.date,
                mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0,0,0,0);

                for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }

            return '';
        }
        $scope.$watchGroup(['model.attendedDate'],function(newValues, oldValues) {
            if (newValues[0])
                 $scope.model.studentDate = newValues[0];
                });
        $scope.model.saveInput = function(){
            var student;
            for(i=0;i<$scope.model.students.length;i++){
                if($scope.model.students[i].name==$scope.model.name){
                    student = $scope.model.students[i];
                    break;
                }
            }
            var req = {'student': student,'value': true};
            $http.post("http://localhost:8080/studentinputs",req).then(function(data){
               alert("Successfully Posted");
            });
        }
    }]);
