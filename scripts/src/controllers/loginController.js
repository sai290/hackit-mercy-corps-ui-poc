login=angular
    .module('app.login', ['ui.router'])
    .config(["$stateProvider", function($stateProvider) {
        $stateProvider.state({
            name: 'login',
            url: '/login',
            templateUrl: 'scripts/src/templates/login.html',
            controller: "loginController",
            resolve: {
            },
        });
    }])
    .controller("loginController", ["$scope", function($scope) {
        $scope.projectName="Conditional Aid - World Bank Page";
        $scope.someData=['',''];
    }]);