beneficiary=angular
    .module('app.beneficiary', ['ui.router', 'app.beneficiaryCreate', 'app.data.beneficiary', 'ui.bootstrap'])
    .config(["$stateProvider", function($stateProvider) {
        $stateProvider.state({
            name: 'beneficiary',
            url: '/beneficiary',
            templateUrl: 'scripts/src/templates/beneficiary.html',
            controller: "beneficiaryController",
            resolve: {
                beneficiaries: ['beneficiaryDataService', function(beneficiaryDataService) {
                    return beneficiaryDataService.getAllBeneficiaries();
                }]},
        });
    }])
    .controller("beneficiaryController", ["$scope", "$sce", "beneficiaries", function($scope,$sce,beneficiaries) {
        $scope.projectName = "Conditional Aid - Student Page";
        $scope.tabs = [

            { title:'View Transactions', content:'Transactions' },
            { title:'Track Aid', content:'Track Aid' },
            { title:'Special Cases', content:'Health Issue or Any Special Case can be requested with proper proof' }
        ];

        $scope.model = {
            name: 'Tabs'
        };
        $scope.model.values = beneficiaries._embedded.beneficiaries;
        $scope.trustHtml = function(content){
            return $sce.trustAsHtml( content );
        }
    }]);