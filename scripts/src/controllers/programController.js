aci=angular
    .module('app.program', ['ui.router','app.data.student', 'app.data.beneficiary', 'app.data.transaction',"googlechart"])
    .config(["$stateProvider", function($stateProvider) {
        $stateProvider.state({
            name: 'program',
            url: '/program',
            templateUrl: 'scripts/src/templates/program.html',
            controller: "programController",
            resolve: {
                beneficiaries: ['beneficiaryDataService', function(beneficiaryDataService) {
                    return beneficiaryDataService.getAllBeneficiaries();
                }],
                students: ['studentDataService', function(studentDataService) {
                    return studentDataService.getAllStudents();
                }],
                nclInfo: ['nclDataService', function(nclDataService) {
                    return nclDataService.getAllNclApis();
                }]
            },
        });
    }]).filter('toArray', function () {
        return function (obj, addKey) {
            if (!angular.isObject(obj)) return obj;
            if ( addKey === false ) {
                return Object.keys(obj).map(function(key) {
                    return obj[key];
                });
            } else {
                return Object.keys(obj).map(function (key) {
                    var value = obj[key];
                    return angular.isObject(value) ?
                        Object.defineProperty(value, '$key', { enumerable: false, value: key}) :
                        { $key: key, $value: value };
                });
            }
        };
    })
    .controller("programController", ["$scope", "$sce",'$http',"$filter", "beneficiaries", "students", "nclInfo", 'nclDataService',"transactionDataService", function($scope,$sce,$http,$filter,beneficiaries,students,nclInfo, nclDataService,transactionDataService) {
        google.charts.load('current', {packages: ['corechart', 'line']});
        //google.charts.setOnLoadCallback(drawBasic);

        $scope.projectName="Sub-Saharan Girls School Incentive Program - Admin Page";
        $scope.programModel = {};
        $scope.showGraph=false;
        $scope.nclIds=[];
        parseIds(nclInfo["_embedded"].program);
        $scope.programModel.nclInfo = nclInfo["_embedded"].program;
        $scope.programModel.delete=function(row) {
            var backLink = row._links.self.href;
            var id = backLink.split("/")[backLink.split("/").length - 1];
            nclDataService.deleteNclApi(id).then(function () {
                var index=$scope.programModel.nclInfo.indexOf(row);
                $scope.programModel.nclInfo.splice(index, 1);
                $state.go("mercyCorps");
            })
            ;
        };
        $scope.toggleSearch = function(){
            $scope.showGraph= !$scope.showGraph;
            if($scope.showGraph)
                document.getElementById('chart_div').style.visibility="hidden";
            var data = new google.visualization.DataTable();
            data.addColumn('number', 'X');
            data.addColumn('number', 'Jenny');

            data.addRows([
                [0, 100],   [1, 50],  [2, 66],  [3, 75],  [4, 79],  [5, 70],
                [6, 73],  [7, 75],  [8, 76],  [9, 76],  [10, 77], [11, 78],
                [12, 79], [13, 80], [14, 81], [15, 82], [16, 82], [17, 83],
                [18, 83], [19, 84], [20, 80], [21, 81], [22, 82], [23, 83],
                [24, 84], [25, 85], [26, 85], [27, 86], [28, 87], [29, 88]
            ]);

            var options = {
                hAxis: {
                    title: 'Days'
                },
                vAxis: {
                    title: 'Attendance Percentage'
                }
            };

            var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

            chart.draw(data, options);
            document.getElementById("chart_div").style.visibility = "visible";
        };
        function parseIds(nclapis){
            $.each(nclapis, function(i, nclapi){
                var backLink = nclapi._links.self.href;
                var id = backLink.split("/")[backLink.split("/").length - 1];
                //alert(id);
                nclapi.id = id;
                $scope.nclIds.push(id);
            });
            //console.log($scope.nclIds)
        }
        $scope.model = [];
        $scope.someData=['',''];
        $scope.model.participants =[];
        $scope.model.report=[];
        $scope.transactions=[];
        $scope.fromdt= new Date();
        $scope.todt = new Date();
        $scope.today = function() {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.clear = function() {
            $scope.dt = null;
        };

        $scope.options = {
            customClass: getDayClass,
            minDate: new Date(),
            showWeeks: true
        };

        // Disable weekend selection
        function disabled(data) {
            var date = data.date,
                mode = data.mode;
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        }

        $scope.toggleMin = function() {
            $scope.options.minDate = $scope.options.minDate ? null : new Date();
        };

        $scope.toggleMin();

        $scope.setDate = function(year, month, day) {
            $scope.dt = new Date(year, month, day);
        };

        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date(tomorrow);
        afterTomorrow.setDate(tomorrow.getDate() + 1);
        $scope.events = [
            {
                date: tomorrow,
                status: 'full'
            },
            {
                date: afterTomorrow,
                status: 'partially'
            }
        ];

        function getDayClass(data) {
            var date = data.date,
                mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0,0,0,0);

                for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }

            return '';
        }

        $scope.showReport = false;
        $scope.viewTransactionsStatus=false;
        $scope.tabs = [

            { title:'View Transactions', content:'' },
            { title:'Track Aid', content:'' }
        ];
        //$scope.programList = [{"name": "Student Aid"},{"name": "Farming Aid"},{"name": "Health Care"},{"name": "Disaster Relief"},{"name": "Awareness"}];
        $scope.programList=[];
        $scope.monthList = [{"name": "January","value":1},{"name": "February","value":2},{"name": "March","value":3},{"name": "April","value":4},{"name": "May","value":5},{"name": "June","value":6},{"name": "July","value":7},{"name": "August","value":8},{"name": "September","value":9},{"name": "October","value":10},{"name": "November","value":11},{"name": "December","value":12}];
        $scope.model.actions = [{"name": "Custom Attendance","value":0},{"name": "Aid Transfer","value":9},{"name": "Conditional Input","value":10},{"name": "Feedback","value":12}];

        $scope.model.save = function(){
            $scope.showReport = true;
        };
        $scope.model.beneficiaries = beneficiaries._embedded.beneficiaries;
        $scope.model.students = students._embedded.student;
        for(i=0;i<$scope.model.beneficiaries.length;i++){
            $scope.model.participants.push({'name': $scope.model.beneficiaries[i].name}) ;
        }
        for(i=0;i<$scope.model.students.length;i++){
            $scope.model.participants.push({'name': $scope.model.students[i].name}) ;
        }
        $scope.model.values=[{'Beneficiary' : 'Alice','Student': 'Adam','conditionStatus':true,'aidTransfer': true,'Receipt': true},{'Beneficiary' : 'Alice','Student': 'Alice','conditionStatus':false,'aidTransfer': false,'Receipt': false},{'Beneficiary' : 'Bob','Student': 'Ashley','conditionStatus':true,'aidTransfer': true,'Receipt': true},{'Beneficiary' : 'Sam','Student': 'Charles','conditionStatus':true,'aidTransfer': true,'Receipt': true},{'Beneficiary' : 'Bryant','Student': 'Jones','conditionStatus':true,'aidTransfer': true,'Receipt': true},{'Beneficiary' : 'Cousins','Student': 'Allen','conditionStatus':true,'aidTransfer': true,'Receipt': true},{'Beneficiary' : 'Alice','Student': 'Adam','conditionStatus':true,'aidTransfer': true,'Receipt': true}];
        $scope.model.ruleSave = function(){
            var url = "http://localhost:8080/conditions";
            request={'name': $scope.model.program,'valueToMeet': $scope.model.percentage};
            $http.post(url,request).then(function (data, status, headers, config) {
                alert("Rule Updated Successfully");
            },function (data, status, headers, config) {
                alert("error");
            });
        }   ;
        function init(){
            var url = "http://localhost:8080/conditions";
            $http.get(url).then(function (response, status, headers, config) {
                $scope.programList=response.data._embedded.conditions;
            },function (data, status, headers, config) {
                alert("error");
            });
        }
        init();
        $scope.model.viewTransactions = function(){
            $scope.viewTransactionsStatus=false;
            $scope.transactions=null;
            transactionDataService.getTransactions().then(function(outputData){
                $scope.viewTransactionsStatus=true;
                $scope.transactions=outputData;
            });
        }
        $scope.model.participant = "Jenny";
        $scope.model.type = "student";
        $scope.fro = new Date(2018,4,1);
        $scope.todt = new Date(2018,4,31);
        $scope.fromdt = new Date(2018,4,1);
        $scope.tod = new Date(2018,4,31);
        $scope.model.action="Custom Attendance";
        $scope.model.searchTransactions = function(){
            $scope.searchTransactionsStatus=false;
            // $scope.fro = new Date(2018,0,1);
            // $scope.tod = new Date();
            //
            var request = {'name': $scope.model.participant,'participantType': $scope.model.type, 'from': $filter('date')($scope.fro, "yyyy-MM-dd"),'to': $filter('date')($scope.todt, "yyyy-MM-dd")};
            console.log(request);
            console.log($scope.fromdt);
            console.log($scope.todt);
            $http.post("http://localhost:8080/api/mercyCorps/search",request).then(function(outputData){
                $scope.searchTransactionsStatus=true;

                $scope.model.searchData=[
                    {
                        "date": "06-01-18",
                        "attendance": "true"
                    },
                    {
                        "date": "06-02-18",
                        "attendance": "false"
                    },
                    {
                        "date": "06-03-18",
                        "attendance": "true"
                    },
                    {
                        "date": "06-04-18",
                        "attendance": "true"
                    },
                    {
                        "date": "06-05-18",
                        "attendance": "true"
                    },
                    {
                        "date": "06-06-18",
                        "attendance": "true"
                    },
                    {
                        "date": "06-07-18",
                        "attendance": "true"
                    },
                    {
                        "date": "06-08-18",
                        "attendance": "true"
                    },
                    {
                        "date": "06-09-18",
                        "attendance": "true"
                    },
                    {
                        "date": "06-10-18",
                        "attendance": "true"
                    },
                    {
                        "date": "06-11-18",
                        "attendance": "true"
                    },
                    {
                        "date": "06-12-18",
                        "attendance": "true"
                    },
                    {
                        "date": "06-13-18",
                        "attendance": "true"
                    },
                    {
                        "date": "06-14-18",
                        "attendance": "true"
                    },
                    {
                        "date": "06-15-18",
                        "attendance": "true"
                    },
                    {
                        "date": "06-16-18",
                        "attendance": "true"
                    },
                    {
                        "date": "06-17-18",
                        "attendance": "true"
                    },
                    {
                        "date": "06-18-18",
                        "attendance": "true"
                    },
                    {
                        "date": "06-19-18",
                        "attendance": "true"
                    },
                    {
                        "date": "06-20-18",
                        "attendance": "true"
                    },
                    {
                        "date": "06-21-18",
                        "attendance": "true"
                    },
                    {
                        "date": "06-22-18",
                        "attendance": "true"
                    },
                    {
                        "date": "06-23-18",
                        "attendance": "true"
                    },
                    {
                        "date": "06-24-18",
                        "attendance": "true"
                    },
                    {
                        "date": "06-25-18",
                        "attendance": "true"
                    },
                    {
                        "date": "06-26-18",
                        "attendance": "true"
                    },
                    {
                        "date": "06-27-18",
                        "attendance": "true"
                    },
                    {
                        "date": "06-28-18",
                        "attendance": "true"
                    },
                    {
                        "date": "06-29-18",
                        "attendance": "true"
                    },
                    {
                        "date": "06-30-18",
                        "attendance": "true"
                    }
                ];
            });
            // transactionDataService.searchTransactions(request).then(function(outputData){
            //     $scope.searchTransactionsStatus=true;
            //     $scope.model.transactions=outputData;
            // });
            //for (var i=0;i<1000;++i) $scope.hostStatus[i]= Math.round(Math.round((Math.random() * 10) * 10) % 2);
        }
        $scope.model.program = "Sub-Saharan Girls School Incentive";
        $scope.model.dateInput ="1";
        $scope.model.generateReport = function(){
            $scope.generateReportStatus=false;
            var request = {"program": $scope.model.program,"dateInput": parseInt($scope.model.dateInput)};
            console.log(request);
            console.log($scope.fromdt);
            console.log($scope.todt);
            $http.post("http://localhost:8080/api/mercyCorps/report",request).then(function(outputData){
                $scope.generateReportStatus=true;
                $scope.model.report=outputData.data;
            });

        }
       // $scope.model.generateReport();
    }]).filter('custom', function() {
        return function(input, search) {
            if (!input) return input;
            if (!search) return input;
            var expected = ('' + search).toLowerCase();
            var result = {};
            angular.forEach(input, function(value, key) {
                var actual = ('' + value).toLowerCase();
                if (actual.indexOf(expected) !== -1) {
                    result[key] = value;
                }
            });
            return result;
        }
    });;
