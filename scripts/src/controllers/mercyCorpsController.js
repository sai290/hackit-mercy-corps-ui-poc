aci=angular
    .module('app.mercyCorps', ['ui.router', 'app.data.student', 'app.data.beneficiary', 'app.data.transaction','app.data.ncl','app.program-detail','googlechart','ui.bootstrap'])
    .config(["$stateProvider", function($stateProvider) {
        $stateProvider.state({
            name: 'mercyCorps',
            url: '/admin',
            templateUrl: 'scripts/src/templates/mercyCorps.html',
            controller: "mercyCorpsController",
            resolve: {
                beneficiaries: ['beneficiaryDataService', function(beneficiaryDataService) {
                    return beneficiaryDataService.getAllBeneficiaries();
                }],
                students: ['studentDataService', function(studentDataService) {
                    return studentDataService.getAllStudents();
                }],
                nclInfo: ['nclDataService', function(nclDataService) {
                    return nclDataService.getAllNclApis();
                }]
            },
        });
    }])
    .controller("mercyCorpsController", ["$scope","$sce",'$http',"$filter", "beneficiaries", "students", "nclInfo", 'nclDataService',"transactionDataService", function($scope,$sce,$http,$filter,beneficiaries,students,nclInfo, nclDataService,transactionDataService) {
        google.charts.load('current', {packages: ['corechart', 'line']});
        //google.charts.setOnLoadCallback(drawBasic);

        $scope.projectName="Conditional Aid - Admin Page";
        $scope.programModel = {};
        $scope.showGraph=false;
        $scope.nclIds=[];
        parseIds(nclInfo["_embedded"].program);
        $scope.programModel.nclInfo = nclInfo["_embedded"].program;
        $scope.programModel.delete=function(row) {
            var backLink = row._links.self.href;
            var id = backLink.split("/")[backLink.split("/").length - 1];
            nclDataService.deleteNclApi(id).then(function () {
                var index=$scope.programModel.nclInfo.indexOf(row);
                $scope.programModel.nclInfo.splice(index, 1);
                $state.go("mercyCorps");
            })
            ;
        };
        $scope.toggleSearch = function(){
            $scope.showGraph=true;
            var data = new google.visualization.DataTable();
            data.addColumn('number', 'X');
            data.addColumn('number', 'Jenny');

            data.addRows([
                [0, 100],   [1, 50],  [2, 66],  [3, 75],  [4, 79],  [5, 70],
                [6, 73],  [7, 75],  [8, 76],  [9, 76],  [10, 77], [11, 78],
                [12, 79], [13, 80], [14, 81], [15, 82], [16, 82], [17, 83],
                [18, 83], [19, 84], [20, 80], [21, 81], [22, 82], [23, 83],
                [24, 84], [25, 85], [26, 85], [27, 86], [28, 87], [29, 88]
            ]);

            var options = {
                hAxis: {
                    title: 'Days'
                },
                vAxis: {
                    title: 'Attendance Percentage'
                }
            };

            var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

            chart.draw(data, options);
            document.getElementById("chart_div").style.visibility = "visible";
        };
        function parseIds(nclapis){
            $.each(nclapis, function(i, nclapi){
                var backLink = nclapi._links.self.href;
                var id = backLink.split("/")[backLink.split("/").length - 1];
                //alert(id);
                nclapi.id = id;
                $scope.nclIds.push(id);
            });
            //console.log($scope.nclIds)
        }
        $scope.model = [];
        $scope.someData=['',''];
        $scope.model.participants =[];
        $scope.model.report=[];
        $scope.fromdt= new Date();
        $scope.todt = new Date();
        $scope.today = function() {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.clear = function() {
            $scope.dt = null;
        };

        $scope.options = {
            customClass: getDayClass,
            minDate: new Date(),
            showWeeks: true
        };

        // Disable weekend selection
        function disabled(data) {
            var date = data.date,
                mode = data.mode;
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        }

        $scope.toggleMin = function() {
            $scope.options.minDate = $scope.options.minDate ? null : new Date();
        };

        $scope.toggleMin();

        $scope.setDate = function(year, month, day) {
            $scope.dt = new Date(year, month, day);
        };

        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date(tomorrow);
        afterTomorrow.setDate(tomorrow.getDate() + 1);
        $scope.events = [
            {
                date: tomorrow,
                status: 'full'
            },
            {
                date: afterTomorrow,
                status: 'partially'
            }
        ];

        function getDayClass(data) {
            var date = data.date,
                mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0,0,0,0);

                for (var i = 0; i < $scope.events.length; i++) {
                    var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

                    if (dayToCheck === currentDay) {
                        return $scope.events[i].status;
                    }
                }
            }

            return '';
        }

        $scope.showReport = false;
        $scope.viewTransactionsStatus=false;
        $scope.tabs = [

            { title:'View Transactions', content:'' },
            { title:'Track Aid', content:'' }
        ];
        //$scope.programList = [{"name": "Student Aid"},{"name": "Farming Aid"},{"name": "Health Care"},{"name": "Disaster Relief"},{"name": "Awareness"}];
        $scope.programList=[];
        $scope.monthList = [{"name": "January","value":1},{"name": "February","value":2},{"name": "March","value":3},{"name": "April","value":4},{"name": "May","value":5},{"name": "June","value":6},{"name": "July","value":7},{"name": "August","value":8},{"name": "September","value":9},{"name": "October","value":10},{"name": "November","value":11},{"name": "December","value":12}];
        $scope.model.actions = [{"name": "Custom Attendance","value":0},{"name": "Aid Transfer","value":9},{"name": "Conditional Input","value":10},{"name": "Feedback","value":12}];

        $scope.model.save = function(){
          $scope.showReport = true;
        };
        $scope.model.beneficiaries = beneficiaries._embedded.beneficiaries;
        $scope.model.students = students._embedded.student;
        for(i=0;i<$scope.model.beneficiaries.length;i++){
            $scope.model.participants.push({'name': $scope.model.beneficiaries[i].name}) ;
        }
        for(i=0;i<$scope.model.students.length;i++){
            $scope.model.participants.push({'name': $scope.model.students[i].name}) ;
        }
        $scope.model.values=[{'Beneficiary' : 'Alice','Student': 'Adam','conditionStatus':true,'aidTransfer': true,'Receipt': true},{'Beneficiary' : 'Alice','Student': 'Alice','conditionStatus':false,'aidTransfer': false,'Receipt': false},{'Beneficiary' : 'Bob','Student': 'Ashley','conditionStatus':true,'aidTransfer': true,'Receipt': true},{'Beneficiary' : 'Sam','Student': 'Charles','conditionStatus':true,'aidTransfer': true,'Receipt': true},{'Beneficiary' : 'Bryant','Student': 'Jones','conditionStatus':true,'aidTransfer': true,'Receipt': true},{'Beneficiary' : 'Cousins','Student': 'Allen','conditionStatus':true,'aidTransfer': true,'Receipt': true},{'Beneficiary' : 'Alice','Student': 'Adam','conditionStatus':true,'aidTransfer': true,'Receipt': true}];
        $scope.model.ruleSave = function(){
            var url = "http://localhost:8080/conditions";
            request={'name': $scope.model.program,'valueToMeet': $scope.model.percentage};
            $http.post(url,request).then(function (data, status, headers, config) {
                alert("Rule Updated Successfully");
            },function (data, status, headers, config) {
                alert("error");
            });
        }   ;
        function init(){
            var url = "http://localhost:8080/conditions";
            $http.get(url).then(function (response, status, headers, config) {
                $scope.programList=response.data._embedded.conditions;
            },function (data, status, headers, config) {
                alert("error");
            });
        }
        init();
        $scope.model.viewTransactions = function(){
            $scope.viewTransactionsStatus=false;
            transactionDataService.getTransactions().then(function(outputData){
                $scope.viewTransactionsStatus=true;
                $scope.model.transactions=outputData;
            });
        }
        $scope.model.searchTransactions = function(){
            $scope.searchTransactionsStatus=false;
            $scope.fro = new Date(2018,0,1);
            $scope.tod = new Date();
            //
            var request = {'name': $scope.model.participant,'participantType': $scope.model.type, 'from': $filter('date')($scope.fro, "yyyy-MM-dd"),'to': $filter('date')($scope.todt, "yyyy-MM-dd")};
            console.log(request);
            console.log($scope.fromdt);
            console.log($scope.todt);
            $http.post("http://localhost:8080/api/mercyCorps/search",request).then(function(outputData){
                $scope.searchTransactionsStatus=true;
                $scope.model.searchData=outputData.data;
            });
            // transactionDataService.searchTransactions(request).then(function(outputData){
            //     $scope.searchTransactionsStatus=true;
            //     $scope.model.transactions=outputData;
            // });
            //for (var i=0;i<1000;++i) $scope.hostStatus[i]= Math.round(Math.round((Math.random() * 10) * 10) % 2);
        }

        $scope.model.generateReport = function(){
            $scope.generateReportStatus=false;
            var request = {"program": $scope.model.program,"dateInput": parseInt($scope.model.dateInput)};
            console.log(request);
            console.log($scope.fromdt);
            console.log($scope.todt);
            $http.post("http://localhost:8080/api/mercyCorps/report",request).then(function(outputData){
                $scope.generateReportStatus=true;
                $scope.model.report=outputData.data;
            });

        }

        //$scope.projectName="Conditional Aid - Funding";
        $scope.someData=['',''];
        $scope.myChartObject = {};

        $scope.myChartObject.type = "PieChart";

        $scope.onions = [
            {v: "Awareness"},
            {v: 300000},
            {f: "$300000"}
        ];

        $scope.myChartObject.data = {"cols": [
            {id: "t", label: "Program", type: "string"},
            {id: "s", label: "Money", type: "number"}
        ], "rows": [
            {c: [
                {v: "Student Aid"},
                {v: 800000},
                {f: "$800000.00"}
            ]},
            {c: $scope.onions},
            {c: [
                {v: "Farming"},
                {v: 3100000},
                {f: "$3100000"}
            ]},
            {c: [
                {v: "Health Care"},
                {v: 700000},
                {f: "$100000"}
            ]},
            {c: [
                {v: "Disaster Relief"},
                {v: 2000000},
                {f: "$2000000"}
            ]}
        ]};

        $scope.myChartObject.options = {
            'title': 'Fund Distribution'
        };
        $scope.myChartObject2 = {};

        $scope.myChartObject2.type = "BarChart";

        $scope.myChartObject2.data = {"cols": [
            {id: "t", label: "Program", type: "string"},
            {id: "s", label: "Money", type: "number"}
        ], "rows": [
            {c: [
                {v: "Student Aid"},
                {v: 800000},
                {f: "$800000.00"}
            ]},
            {c: $scope.onions},
            {c: [
                {v: "Farming"},
                {v: 3100000},
                {f: "$3100000"}
            ]},
            {c: [
                {v: "Health Care"},
                {v: 700000},
                {f: "$100000"}
            ]},
            {c: [
                {v: "Disaster Relief"},
                {v: 2000000},
                {f: "$2000000"}
            ]}
        ]};

        $scope.myChartObject2.options = {
            'title': 'Fund Distribution'
        };
    }]);
