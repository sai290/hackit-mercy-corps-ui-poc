aci=angular
    .module('app.createStudent', ['ui.router','app.data.student','app.data.beneficiary', 'ui.bootstrap'])
    .config(["$stateProvider", function($stateProvider) {
        $stateProvider.state({
            name: 'createStudent',
            url: '/createStudent',
            templateUrl: 'scripts/src/templates/studentCreate.html',
            controller: "studentCreateController",
            resolve: {
                beneficiaries: ['beneficiaryDataService', function(beneficiaryDataService) {
                    return beneficiaryDataService.getAllBeneficiaries();
                }]
            },
        });
    }])
    .controller("studentCreateController", ["$scope", "$http", "beneficiaries", function($scope,$http,beneficiaries) {
        $scope.projectName = "Conditional Aid - Student Page";
        $scope.beneficiaryList = beneficiaries._embedded.beneficiaries;
        $scope.programList = [];
        function init(){
            var url = "http://localhost:8080/conditions";
            $http.get(url).then(function (response, status, headers, config) {
                $scope.programList=response.data._embedded.conditions;
            },function (data, status, headers, config) {
                alert("error");
            });
        }
        init();
    }]);