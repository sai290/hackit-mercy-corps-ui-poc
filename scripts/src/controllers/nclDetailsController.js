angular
    .module('app.program-detail', ['ui.router', 'app.data.ncl','ngTable'])
    .config(["$stateProvider", function($stateProvider) {
        $stateProvider.state({
            name: 'program-edit',
            url: '/program-details/:api',
            templateUrl: 'scripts/src/templates/program-edit.html',
            controller: "nclDetailsController",
            resolve: {
                nclApiDetails: ['nclDataService', "$stateParams", function(nclDataService, $stateParams) {
                    if($stateParams.api != null && $stateParams.api != '')
                        return nclDataService.getNCLApiById($stateParams.api);
                    else
                        return null;
                }]},
        });
    }])
    .controller("nclDetailsController", ["$scope", "$state", "nclApiDetails", 'nclDataService', function($scope, $state, nclApiDetails, nclDataService) {
        $scope.model = {};
        var _nclApiParameters=new Array({'name': '', 'inputType': '', 'required': true});
        var _conditionStatements=new Array({'name': '','inputType': '','aggregateType':'EXTERNAL','field':'','value':''});
        $scope.message='List by Order';
        //console.log(nclApiDetails);
        $scope.model={
            api: nclApiDetails==null ? '' : nclApiDetails.api,
            category: nclApiDetails==null ? 'Wellfare' : nclApiDetails.category,
            fund: nclApiDetails==null ? '' : nclApiDetails.fund,
            frequency: nclApiDetails==null ? '' : nclApiDetails.frequency,
            nclApiParameters: nclApiDetails==null ?  _nclApiParameters: nclApiDetails.nclApiParameters,
            conditionStatements: nclApiDetails==null ?  _conditionStatements: nclApiDetails.conditionStatements,
            inputTypes: ['STRING', 'INTEGER', 'NUMBER', 'BOOL', 'DATE','IMAGE', 'FILE'],
            operationTypes: ['SUM',
                'AVERAGE',
                'MAX',
                'MIN',
                'MEAN','EQUALS','EXTERNAL'],
            conditionTypes:['EQUALS','LESSTHAN',
                'GREATERTAHN',
                'LESSTHANOREQUAL',
                'GREATERTAHNOREQUAL',
                'CONTAINS',
                'MANUAL',
                'OTHER'],
            frequencyTypes: ['One Time','Daily','Weekly','Bi-Weekly','Monthly','Quarterly','Half-Yearly','Yearly']
        };

        //$scope.model.nclInfo = nclInfo["_embedded"].nclApi;
        $scope.model.save=function() {
            var dto = {
                api: $scope.model.api,
                nclApiParameters: $scope.model.nclApiParameters,
                conditionStatements: $scope.model.conditionStatements,
                category: $scope.model.category,
                fund: $scope.model.fund,
                frequency: $scope.model.frequency
            }
            if(nclApiDetails == null) {
                nclDataService.createNclApi(dto).then(function(data){
                    $state.go("mercyCorps");
                });
            } else {
                var backLink = nclApiDetails._links.self.href;
                var id = backLink.split("/")[backLink.split("/").length - 1];

                nclDataService.updateNclApi(id, dto).then(function(data){
                    $state.go("mercyCorps");
                });
            }
        };
        $scope.model.cancel=function () {
            $state.go("mercyCorps");
        };
        $scope.model.addParam=function () {
            $scope.model.nclApiParameters.push({'name': '', 'inputType': '', 'required': true});
        };
        $scope.model.deleteParam=function ($index) {
            //debugger;
            $scope.model.nclApiParameters.splice($index);
            $scope.apply();
        };
        $scope.model.addDef=function () {
            $scope.model.conditionStatements.push({'name': '','inputType': '','aggregateType':'EXTERNAL','field':'','value':''});
        };
        $scope.model.deleteDef=function ($index) {
            //debugger;
            $scope.model.conditionStatements.splice($index);
            $scope.apply();
        };

    }]);

