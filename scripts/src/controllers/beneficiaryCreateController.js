beneficiaryCreate=angular
    .module('app.beneficiaryCreate', ['ui.router', 'app.data.beneficiary','app.mercyCorps','ui.bootstrap'])
    .config(["$stateProvider", function($stateProvider) {
        $stateProvider.state({
            name: 'beneficiaryCreate',
            url: '/beneficiaryCreate',
            templateUrl: 'scripts/src/templates/beneficiaryCreate.html',
            controller: "beneficiaryCreateController",
            resolve: {
            },
        });
    }])
    .controller("beneficiaryCreateController", ["$scope", "$state", "beneficiaryDataService",function($scope,$state, beneficiaryDataService) {
        $scope.projectName = "Conditional Aid - Enroll New Beneficiary";
        $scope.model={};
        $scope.model.save = function(){
            var req = {
                'name': $scope.model.name,
                'phone': $scope.model.phone,
                'aid': {
                    'bankName': $scope.model.bankName,
                    'accountNo': $scope.model.accountNo
                }

            };
            beneficiaryDataService.createBenificiary(req).then(function(){
                $state.go('mercyCorps')
            });
        }

    }]);