/**
 * Created by ppatapan on 5/10/2018.
 */
angular
    .module('app.data.worldBank', ["app.config"])
    .factory('worldBankDataService', ['$http', "apiBaseUrl", "$q", function($http, apiBaseUrl, $q){
        var factory = {};

        factory.getManagers = function() {
            var deferred = $q.defer();

            $.ajax({
                url: apiBaseUrl + "/worldBank/managers",
                type: "GET",
                dataType: "json",
                contentType: "application/json",
                beforeSend: function () {

                },
                success: function (data) {
                    deferred.resolve(data);
                },
                complete: function () {

                },
                error: function (err) {
                    deferred.reject(err);
                }
            });

            return deferred.promise;
        };

        factory.getManagerByName = function(name) {
            var deferred = $q.defer();
            $.ajax({
                url: apiBaseUrl + "/worldBank/manager/findByName?name=" + name,
                type: "GET",
                dataType: "json",
                contentType: "application/json",
                beforeSend: function () {

                },
                success: function (data) {

                    deferred.resolve(data);
                },
                complete: function () {

                },
                error: function (err) {
                    deferred.reject(err);
                }
            });

            return deferred.promise;
        };

        factory.getManagerById = function(id) {
            var deferred = $q.defer();
            $.ajax({
                url: apiBaseUrl + "/worldBank/manager/findById?api=" + id,
                type: "GET",
                dataType: "json",
                contentType: "application/json",
                beforeSend: function () {

                },
                success: function (data) {
                    deferred.resolve(data);
                },
                complete: function () {

                },
                error: function (err) {
                    deferred.reject(err);
                }
            });

            return deferred.promise;
        };


        factory.createManager=function (manager) {
            return $http.post(apiBaseUrl + "/worldBank/manger/", manager);
        }

        factory.updateManager=function (id, manager) {
            return $http.patch(apiBaseUrl + "/worldBank/manager/" + id, manager);
        };

        factory.deleteManager=function (id) {
            return $http.delete(apiBaseUrl + "/worldBank/manager/" + id);
        };

        function nonCachedGet(url) {
            return $http({
                method: 'GET',
                url: url,
                cache: false
            });
        }

        return factory;
    }]);