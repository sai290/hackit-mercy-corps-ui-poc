/**
 * Created by ppatapan on 5/10/2018.
 */
angular
    .module('app.data.mercyCorps', ["app.config"])
    .factory('mercyCorpDataService', ['$http', "apiBaseUrl", "$q", function($http, apiBaseUrl, $q){
        var factory = {};

        factory.getManagers = function() {
            var deferred = $q.defer();

            $.ajax({
                url: apiBaseUrl + "/managers",
                type: "GET",
                dataType: "json",
                contentType: "application/json",
                beforeSend: function () {

                },
                success: function (data) {
                    deferred.resolve(data);
                },
                complete: function () {

                },
                error: function (err) {
                    deferred.reject(err);
                }
            });

            return deferred.promise;
        };

        factory.getManagerByName = function(name) {
            var deferred = $q.defer();
            $.ajax({
                url: apiBaseUrl + "/mercyCorps/manager/findByName?name=" + name,
                type: "GET",
                dataType: "json",
                contentType: "application/json",
                beforeSend: function () {

                },
                success: function (data) {

                    deferred.resolve(data);
                },
                complete: function () {

                },
                error: function (err) {
                    deferred.reject(err);
                }
            });

            return deferred.promise;
        };

        factory.getManagerById = function(id) {
            var deferred = $q.defer();
            $.ajax({
                url: apiBaseUrl + "/mercyCorps/manager/findById?api=" + id,
                type: "GET",
                dataType: "json",
                contentType: "application/json",
                beforeSend: function () {

                },
                success: function (data) {
                    deferred.resolve(data);
                },
                complete: function () {

                },
                error: function (err) {
                    deferred.reject(err);
                }
            });

            return deferred.promise;
        };


        factory.createManager=function (student) {
            return $http.post(apiBaseUrl + "/manger/", student);
        }

        factory.updateManager=function (id, student) {
            return $http.patch(apiBaseUrl + "/manager/" + id, student);
        };

        factory.deleteManager=function (id) {
            return $http.delete(apiBaseUrl + "/manager/" + id);
        };

        function nonCachedGet(url) {
            return $http({
                method: 'GET',
                url: url,
                cache: false
            });
        }

        return factory;
    }]);