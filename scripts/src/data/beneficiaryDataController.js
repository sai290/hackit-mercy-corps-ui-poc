angular
    .module('app.data.beneficiary', ["app.config"])
    .factory('beneficiaryDataService', ['$http', "apiBaseUrl", "$q", function($http, apiBaseUrl, $q){
        var factory = {};

        factory.getAllBeneficiaries = function() {
            var deferred = $q.defer();

            $.ajax({
                url: apiBaseUrl + "/beneficiaries?sort=name",
                type: "GET",
                dataType: "json",
                contentType: "application/json",
                beforeSend: function () {

                },
                success: function (data) {
                    deferred.resolve(data);
                },
                complete: function () {

                },
                error: function (err) {
                    deferred.reject(err);
                }
            });

            return deferred.promise;
        };

        factory.getBeneficiaryByName = function(name) {
            var deferred = $q.defer();
            $.ajax({
                url: apiBaseUrl + "/beneficiaries?name=" + encodeURIComponent(name),
                type: "GET",
                dataType: "json",
                contentType: "application/json",
                beforeSend: function () {

                },
                success: function (data) {

                    deferred.resolve(data);
                },
                complete: function () {

                },
                error: function (err) {
                    deferred.reject(err);
                }
            });

            return deferred.promise;
        };

        factory.getBeneficiaryById = function(id) {
            var deferred = $q.defer();
            $.ajax({
                url: apiBaseUrl + "/beneficiaries/search/findById?api=" + id,
                type: "GET",
                dataType: "json",
                contentType: "application/json",
                beforeSend: function () {

                },
                success: function (data) {
                    deferred.resolve(data);
                },
                complete: function () {

                },
                error: function (err) {
                    deferred.reject(err);
                }
            });

            return deferred.promise;
        };

        factory.createBenificiary=function (benificiary) {
            return $http.post(apiBaseUrl + "/beneficiaries/", benificiary);
        }

        factory.updateStudent=function (id, student) {
            return $http.patch(apiBaseUrl + "/student/" + id, student);
        };

        factory.deleteStudent=function (id) {
            return $http.delete(apiBaseUrl + "/student/" + id);
        };

        function nonCachedGet(url) {
            return $http({
                method: 'GET',
                url: url,
                cache: false
            });
        }

        return factory;
    }]);