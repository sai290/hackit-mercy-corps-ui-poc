angular
    .module('app.data.student', ["app.config"])
    .factory('studentDataService', ['$http', "apiBaseUrl", "$q", function($http, apiBaseUrl, $q){
        var factory = {};

        factory.getAllStudents = function() {
            var deferred = $q.defer();

            $.ajax({
                url: apiBaseUrl + "/student",
                type: "GET",
                dataType: "json",
                contentType: "application/json",
                beforeSend: function () {

                },
                success: function (data) {
                    deferred.resolve(data);
                },
                complete: function () {

                },
                error: function (err) {
                    deferred.reject(err);
                }
            });

            return deferred.promise;
        };

        factory.getStudentByName = function(name) {
            var deferred = $q.defer();
            $.ajax({
                url: apiBaseUrl + "/student?name=" + name,
                type: "GET",
                dataType: "json",
                contentType: "application/json",
                beforeSend: function () {

                },
                success: function (data) {

                    deferred.resolve(data);
                },
                complete: function () {

                },
                error: function (err) {
                    deferred.reject(err);
                }
            });

            return deferred.promise;
        };

        factory.saveInput = function(request) {
            var deferred = $q.defer();
            $.ajax({
                url: apiBaseUrl + "/studentinputs",
                type: "POST",
                data: request,
                dataType: "json",
                contentType: "application/json",
                beforeSend: function () {

                },
                success: function (data) {
                    deferred.resolve(data);
                },
                complete: function () {

                },
                error: function (err) {
                    deferred.reject(err);
                }
            });

            return deferred.promise;
        };

        factory.getStudentById = function(id) {
            var deferred = $q.defer();
            $.ajax({
                url: apiBaseUrl + "/student/search/findById?api=" + id,
                type: "GET",
                dataType: "json",
                contentType: "application/json",
                beforeSend: function () {

                },
                success: function (data) {
                    deferred.resolve(data);
                },
                complete: function () {

                },
                error: function (err) {
                    deferred.reject(err);
                }
            });

            return deferred.promise;
        };

        factory.createStudent=function (student) {
            return $http.post(apiBaseUrl + "/student/", student);
        }

        factory.updateStudent=function (id, student) {
            return $http.patch(apiBaseUrl + "/student/" + id, student);
        };

        factory.deleteStudent=function (id) {
            return $http.delete(apiBaseUrl + "/student/" + id);
        };

        function nonCachedGet(url) {
            return $http({
                method: 'GET',
                url: url,
                cache: false
            });
        }

        return factory;
    }]);