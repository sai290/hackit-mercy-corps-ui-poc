angular
    .module('app.data.ncl', ["app.config"])
    .factory('nclDataService', ['$http', "apiBaseUrl", "$q", function($http, apiBaseUrl, $q){
        var factory = {};

        factory.getAllNclApis = function() {
            var deferred = $q.defer();

            $.ajax({
                url: apiBaseUrl + "/program",
                type: "GET",
                dataType: "json",
                contentType: "application/json",
                beforeSend: function () {

                },
                success: function (data) {
                    deferred.resolve(data);
                },
                complete: function () {

                },
                error: function (err) {
                    deferred.reject(err);
                }
            });

            return deferred.promise;
        };

        factory.getNCLApiByName = function(name) {
            var deferred = $q.defer();
            $.ajax({
                url: apiBaseUrl + "/program/search/findByApi?api=" + name,
                type: "GET",
                dataType: "json",
                contentType: "application/json",
                beforeSend: function () {

                },
                success: function (data) {

                    deferred.resolve(data);
                },
                complete: function () {

                },
                error: function (err) {
                    deferred.reject(err);
                }
            });

            return deferred.promise;
        };

        factory.getNCLApiById = function(id) {
            var deferred = $q.defer();
            $.ajax({
                url: apiBaseUrl + "/program/" + id,
                type: "GET",
                dataType: "json",
                contentType: "application/json",
                beforeSend: function () {

                },
                success: function (data) {
                    deferred.resolve(data);
                },
                complete: function () {

                },
                error: function (err) {
                    deferred.reject(err);
                }
            });

            return deferred.promise;
        };

        factory.createNclApi=function (nclApi) {
            return $http.post(apiBaseUrl + "/program/", nclApi);
        }

        factory.updateNclApi=function (id, nclApi) {
            return $http.patch(apiBaseUrl + "/program/" + id, nclApi);
        };

        factory.deleteNclApi=function (id) {
            return $http.delete(apiBaseUrl + "/program/" + id);
        };

        function nonCachedGet(url) {
            return $http({
                method: 'GET',
                url: url,
                cache: false
            });
        }

        return factory;
    }]);