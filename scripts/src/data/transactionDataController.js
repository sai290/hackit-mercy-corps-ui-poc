angular
    .module('app.data.transaction', ["app.config"])
    .factory('transactionDataService', ['$http', "apiBaseUrl", "$q", function($http, apiBaseUrl, $q){
        var factory = {};

        factory.getTransactions = function() {
            var deferred = $q.defer();

            $.ajax({
                url: apiBaseUrl + "/api/mercyCorps/transactions",
                type: "GET",
                dataType: "json",
                contentType: "application/json",
                beforeSend: function () {

                },
                success: function (data) {
                    deferred.resolve(data);
                },
                complete: function () {

                },
                error: function (err) {
                    deferred.reject(err);
                }
            });

            return deferred.promise;
        };

        factory.searchTransactions = function(req) {
            var deferred = $q.defer();

            $.ajax({
                url: apiBaseUrl + "/api/mercyCorps/search?searchRequest"+req,
                type: "GET",
                dataType: "json",
                contentType: "application/json",
                beforeSend: function () {

                },
                success: function (data) {
                    deferred.resolve(data);
                },
                complete: function () {

                },
                error: function (err) {
                    deferred.reject(err);
                }
            });

            return deferred.promise;
        };

        factory.generateReport = function(req) {
            var deferred = $q.defer();

            $.ajax({
                url: apiBaseUrl + "/api/mercyCorps/report",
                type: "POST",
                data: req,
                dataType: "json",
                contentType: "application/json",
                beforeSend: function () {

                },
                success: function (data) {
                    deferred.resolve(data);
                },
                complete: function () {

                },
                error: function (err) {
                    deferred.reject(err);
                }
            });

            return deferred.promise;
        };
        factory.getTransactionById = function(id) {
            var deferred = $q.defer();
            $.ajax({
                url: apiBaseUrl + "/transaction/search/findById?api=" + id,
                type: "GET",
                dataType: "json",
                contentType: "application/json",
                beforeSend: function () {

                },
                success: function (data) {
                    deferred.resolve(data);
                },
                complete: function () {

                },
                error: function (err) {
                    deferred.reject(err);
                }
            });

            return deferred.promise;
        };

        factory.createTransaction=function (transaction) {
            return $http.post(apiBaseUrl + "/transaction/", student);
        }

        factory.updateTransaction=function (id, transaction) {
            return $http.patch(apiBaseUrl + "/student/" + id, student);
        };

        factory.deleteTransaction=function (id) {
            return $http.delete(apiBaseUrl + "/Transaction/" + id);
        };

        function nonCachedGet(url) {
            return $http({
                method: 'GET',
                url: url,
                cache: false
            });
        }

        return factory;
    }]);